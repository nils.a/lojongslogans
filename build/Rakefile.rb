# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
require "rubygems"
gem 'rubyzip', '=1.0.0'
require "fileutils"
require "zip"
require "./lib/crxmake.rb"
include FileTest

$buildDir = File.dirname(__FILE__)
$sourceDir = "#{$buildDir}/../source/"
$chromeAppSource = "#{$sourceDir}ChromeApp/"
$winGadgetSource = "#{$sourceDir}windows_gadget/"
$plasmoidSource = "#{$sourceDir}plasmoid/"
$binDir = "#{$buildDir}/../bin"

desc "Compiles all"
task :default => [:clean, :winGadget, :plasmoid, :chromeApp]

desc "Prepares the bin directory for a new build"
task :clean do
  rm_rf($binDir)
  Dir.mkdir $binDir unless exists?($binDir)
end

desc "KDE Plasmoid"
task :plasmoid do
  Zip::File.open("#{$binDir}/lojongSlogans.plasmoid", Zip::File::CREATE) do |zipfile|
    Dir[File.join($plasmoidSource, '**', '**')].each do |file|
      zipfile.add(file.sub($plasmoidSource, ''), file)
    end
  end
end

desc "The Chrome app howto-task"
task :chromeApp do
  puts "the ChromeApp can not automatically be build."
  puts " If you want to install it locally, simply "
  puts "  open \"chrome://chrome/extensions/\" in Chrome,"
  puts "  activate the developer mode and then "
  puts "  use the \"Load unpacked extension…\" function."
  puts "  see http://developer.chrome.com/extensions/getstarted.html#unpacked"
  puts "  for information"
  puts " If you want to build a crx/zip to deploy you'll need"
  puts "  a private RSA-Key to sign."
  puts "  use rake [chromeAppCrx|chromeAppZip] chromeAppKey=path/to/private/key.pem" 
end

def get_abs_path(rel_path)
  abs_path = Pathname.new(rel_path).realpath.to_s
  puts rel_path + " => " + abs_path
  return abs_path
end

def make_chromeApp(type)
  if type == "crx"
    delegate = "make"
  elsif type == "zip"
    delegate = "zip"
  else
    raise "only 'crx' and 'zip' are possible values!"
  end

  key = ENV['chromeAppKey']
  if key.to_s.empty?
    raise "Error: no chromeAppKey set."
  end
  key = get_abs_path(key)
  puts "generating "+ type +" using private key: " + key
  CrxMake.send( delegate,
    :ex_dir => $chromeAppSource,
    :pkey   => key,
    :zip_output => "#{$binDir}/LojongSloganChromeApp." + type,
    :verbose => true,
    :ignorefile => /\.swp/,
    :ignoredir => /\.(?:svn|git|cvs)/
  )
end

desc "The Chrome app as crx"
task :chromeAppCrx do
  make_chromeApp("crx")
end

desc "The Chrome app as zip"
task :chromeAppZip do
  make_chromeApp("zip")
end

desc "The Windows (Vista, 7) Gadget"
task :winGadget do
  Zip::File.open("#{$binDir}/lojongSlogans.gadget", Zip::File::CREATE) do |zipfile|
    Dir[File.join($winGadgetSource, '**', '**')].each do |file|
      zipfile.add(file.sub($winGadgetSource, ''), file)
    end
  end
end

