# -*- coding: UTF-8 -*-
#
# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
__author__ = "Nils Andresen"
__copyright__ = ["Copyright 2013, Nils Andresen"]
__license__ = "GPL"

from django.db import models

import datetime
import random

from ljslogans import Slogans

class Slogan(models.Model):
    slogan = models.CharField(max_length=254)
    date = models.DateField(unique=True)
    sloganid = models.IntegerField()
    
    def __unicode__(self):
        return u'%s' % self.slogan
    
    @staticmethod
    def sloganOfTheDay(date=datetime.date.today()):
        slogan = Slogan.objects.filter(date__exact=date)[:1]
        if len(slogan) == 1:
            return slogan[0]
    
        rndslogan = Slogan._randomSlogan()
        slogan = Slogan(date = date, slogan = rndslogan[1], sloganid = rndslogan[0])
        slogan.save()
        return slogan
    
    @staticmethod
    def _randomSlogan():
        num = random.randint(0, len(Slogans.SLOGANS) - 1)
        slogan = Slogans.SLOGANS[num]
        return (num + 1, slogan)        
    
    class Meta:
        verbose_name_plural = "Slogans"
        verbose_name = "Slogan"

