# -*- coding: UTF-8 -*-
#
# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
__author__ = "Nils Andresen"
__copyright__ = ["Copyright 2013, Nils Andresen"]
__license__ = "GPL"

from django.http import Http404
from django.shortcuts import HttpResponse

from datetime import date, timedelta
import simplejson as json

from ljslogans import Slogans
from ljslogans.models import Slogan

def listAll(request):
    l = []
    for i in range(len(Slogans.SLOGANS)):
        l.append({"id": i+1, "slogan": Slogans.SLOGANS[i]})

    return HttpResponse(json.dumps(l), mimetype="application/json")

def listOne(request, num):
    try:
        num = int(num)
        if num < 1 or num > (len(Slogans.SLOGANS)):
            return HttpResponse(json.dumps({"error":"No Slogan No. %s" % num}), mimetype="application/json")
    except ValueError:
        raise Http404()
    
    return HttpResponse(json.dumps({"slogan":Slogans.SLOGANS[num-1], "id": num}), mimetype="application/json")

def today(request):
    return _sloganOfTheDay(request, date.today());

def day(request, year, month, day):
    try:
        theDate = date(int(year), int(month), int(day))
        count = 1
        if request.GET.has_key("count"):
            count = int(request.GET["count"])
        return _sloganOfTheDay(request, theDate, count);
    except ValueError:
        raise Http404()

def _sloganOfTheDay(request, date, count=1):
    slogans = []
    for i in range(count):
        d = date + timedelta(days=i)
        sl = Slogan.sloganOfTheDay(d)
        slogans.append({
            "slogan" : sl.slogan,
            "id": sl.sloganid,
            "date": sl.date.isoformat()})
    return HttpResponse(json.dumps(slogans), mimetype="application/json")


