# -*- coding: UTF-8 -*-
#
# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
__author__ = "Nils Andresen"
__copyright__ = ["Copyright 2013, Nils Andresen"]
__license__ = "GPL"

from django.shortcuts import render_to_response
from django.template import RequestContext

from ljslogans.models import Slogan

def mainPage(request):
    slogan = Slogan.sloganOfTheDay() 
    return render_to_response(
        'simple.html', 
        {"slogan":slogan.slogan},
        context_instance=RequestContext(request))

def gadget(request):
    return render_to_response(
        'gadget.xml', 
        {"server" : request.get_host()},
        context_instance=RequestContext(request))
    
def gadgetContent(request):
    slogan = Slogan.sloganOfTheDay() 
    return render_to_response(
        'gadget-content.html', 
        {"slogan":slogan.slogan},
        context_instance=RequestContext(request))