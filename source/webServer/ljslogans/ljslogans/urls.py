# -*- coding: UTF-8 -*-
#
# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
__author__ = "django-admin"
__copyright__ = ["Copyright 2013, Nils Andresen"]
__license__ = "GPL"

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.simple import direct_to_template, redirect_to
from ljslogans import settings

admin.autodiscover()

urlpatterns = patterns('ljslogans.views.json',
    url(r'^AllSlogans$', 'listAll', name='json.listAll'),
    url(r'^AllSlogans/(?P<num>\d+)$', 'listOne', name='json.listOne'),
    url(r'^Slogan$', 'today', name='json.today'),
    url(r'^Slogan/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)$', 'day', name='json.day'),
)

urlpatterns += patterns('ljslogans.views.simple',
    url(r'^$', 'mainPage', name='simple.mainPage'),
    url(r'^gadget\.xml$', 'gadget', name='simple.gadget'),
    url(r'^gadget-content\.html$', 'gadgetContent', name='simple.gadgetContent'),
)

urlpatterns += patterns('',
    url(r'^media/(?P<path>images/.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
    }),
    url(r'^robots\.txt$', direct_to_template, {'template': 'robots.txt', 'mimetype': 'text/plain'}),
    url(r'^favicon\.ico$', redirect_to, {'url': '/media/images/favicon.ico'}),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
