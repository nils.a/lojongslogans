# -*- coding: UTF-8 -*-
#
# Copyright 2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
__author__ = "Nils Andresen"
__copyright__ = ["Copyright 2013, Nils Andresen"]
__license__ = "GPL"

# slogans from Wikipedia...
SLOGANS = [ "1. First, train in the preliminaries.", 
            "2. Regard all dharmas as dreams.",
            "3. Examine the nature of unborn awareness.",
            "4. Self-liberate even the antidote.",
            "5. Rest in the nature of alaya, the essence.",
            "6. In postmeditation, be a child of illusion.",
            "7. Sending and taking should be practiced alternately. These two should ride the breath.",
            "8. Three objects, three poisons, three roots of virtue.",
            "9. In all activities, train with slogans.",
            "10. Begin the sequence of sending and taking with yourself.",
            "11. When the world is filled with evil, transform all mishaps into the path of bodhi.",
            "12. Drive all blames into one.",
            "13. Be grateful to everyone.",
            "14. Seeing confusion as the four kayas is unsurpassable shunyata protection.",
            "15. Four practices are the best of methods.",
            "16. Whatever you meet unexpectedly, join with meditation.",
            "17. Practice the five strengths, the condensed heart instructions.",
            "18. The mahayana instruction for ejection of consciousness at death is the five strengths: how you conduct yourself is important.",
            "19. All dharma agrees at one point.",
            "20. Of the two witnesses, hold the principal one.",
            "21. Always maintain only a joyful mind.",
            "22. If you can practice even when distracted, you are well trained.",
            "23. Always abide by the three basic principles.",
            "24. Change your attitude, but remain neutral.",
            "25. Don't talk about injured limbs.",
            "26. Don't ponder others.",
            "27. Work with the greatest defilements first.",
            "28. Abandon any hope of fruition.",
            "29. Abandon poisonous food.",
            "30. Don't be so predictable.",
            "31. Don't malign others.",
            "32. Don't wait in ambush.",
            "33. Don't bring things to a painful point.",
            "34. Don't transfer the ox's load to the cow.",
            "35. Don't try to be the fastest.",
            "36. Don't act with a twist.",
            "37. Don't make gods into demons.",
            "38. Don't seek others' pain as the limbs of your own happiness.",
            "39. All activities should be done with one intention.",
            "40. Correct all wrongs with one intention.",
            "41. Two activities: one at the beginning, one at the end.",
            "42. Whichever of the two occurs, be patient.",
            "43. Observe these two, even at the risk of your life.",
            "44. Train in the three difficulties.",
            "45. Take on the three principal causes.",
            "46. Pay heed that the three never wane.",
            "47. Keep the three inseparable.",
            "48. Train without bias in all areas. It is crucial always to do this pervasively and wholeheartedly.",
            "49. Always meditate on whatever provokes resentment.",
            "50. Don't be swayed by external circumstances.",
            "51. This time, practice the main points.",
            "52. Don't misinterpret.",
            "53. Don't vacillate.",
            "54. Train wholeheartedly.",
            "55. Liberate yourself by examining and analyzing.",
            "56. Don't wallow in self-pity.",
            "57. Don't be jealous.",
            "58. Don't be frivolous.",
            "59. Don't expect applause."]