/*
# Copyright 2012-2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
*/
document.onreadystatechange = function() {    
    if(document.readyState=="complete") {
        timeout.value = Lojong.Timeout();
        slogan.value = Lojong.LastSlogan();
        update.value = Lojong.LastUpdate();
    }        
}

System.Gadget.onSettingsClosing = function (event) {
    if (event.closeAction == event.Action.commit) {
        Lojong.resetErrors();

        if (timeout.value === "") {
            $("#timeout").css("error");
            Lojong.addError("Timeout can not be empty.");
            event.cancel = true;
            return;
        }

        if (!/[1-9][0-9]*/.test(timeout.value)) {
            $("#timeout").css("error");
            Lojong.addError("Timeout can only be an integer.");
            event.cancel = true;
            return;
        }

        $("#timeout").removeClass("error");
        $("#slogan").removeClass("error");
        $("#update").removeClass("error");

        Lojong.Timeout(timeout.value);
        Lojong.LastSlogan(slogan.value);
        Lojong.LastUpdate(update.value);
        event.cancel = false;
    }
}

var Lojong = Lojong || {};
(function (lj) { 
    lj.addError = function(text) {
        var div = $("#errors");
        div.append(text + "\n");   
        div.height(div.height() + 20); 
    }; 

    lj.resetErrors = function(text) {
        var div = $("#errors");
        div.text("");   
        div.height(0); 
    }; 

})(Lojong);