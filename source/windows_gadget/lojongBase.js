/*
# Copyright 2012-2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
*/
var Lojong = Lojong || {};

(function(lj) {
    lj.Timeout = function(newTimeout) {
        var toKey="timeout";
        if(arguments.length==0) {
            var timoutInHours = System.Gadget.Settings.read(toKey);
            if(timoutInHours == "")
            {
                timoutInHours = 1;
            } 
            
            return timoutInHours;
        }
        
        System.Gadget.Settings.write(toKey, parseInt(newTimeout));
    };
    
    lj.LastSlogan = function(slogan) {
        var slKey="slogan";
        if(arguments.length==0) {
            slogan = System.Gadget.Settings.read(slKey);            
            if(slogan == "")
            {
                slogan = "fetching slogan from server";
            } 

            return slogan;
        }
        
        System.Gadget.Settings.write(slKey, slogan);
    }
    
    lj.LastUpdate = function(update) {
        var upKey="update";
        if(arguments.length==0) {
            update = System.Gadget.Settings.read(upKey);
            
            if(update == "")
            {
                update = "never...";
            } 

            return update;
        }
        
        System.Gadget.Settings.write(upKey, update);
    }
    
    lj.updateSlogan = function() {
        lj.updateSloganOnce();
        window.setTimeout(lj.updateSlogan, lj.Timeout() * 3600000);
    };
    
    lj.updateSloganFromLast = function(errors) {
        slogan.innerText = lj.LastSlogan();
        var last = lj.LastUpdate();
        if(arguments.length==1) {
            last += " and errors since then.";
        }
        lastupdate.innerText = last;                
    }
    
    lj.updateSloganOnce = function() {
        var today = new Date()
        var year = today.getFullYear();
        var month = today.getMonth() + 1
        var day = today.getDate()
        var url = "http://lojong.nils-andresen.de/Slogan/"+year+"/"+month+"/"+day;
        
        $.ajax({
            url: url,
            dataType: 'json',
            success: function(data){
                slogan.innerText = data[0].slogan;
                lastupdate.innerText = today.toLocaleString();
                lj.LastUpdate(today.toLocaleString());
                lj.LastSlogan(data[0].slogan);
            },
            error: function(jqXHR, textStatus, errorThrown){
                // slogan.innerHtml = "error: <br />" + textStatus + "<br />" + errorThrown;
                // lastupdate.innerText = today.toLocaleString();
                lj.updateSloganFromLast(true);
            } 
        });        
    };
})(Lojong);


