/*
# Copyright 2012-2013 Nils Andresen
#
# This file is part of LojongSlogans.
#
# LojongSlogans is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of 
# the License, or (at your option) any later version.
#
# LojongSlogans is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with LojongSlogans. If not, see http://www.gnu.org/licenses/.
*/
document.onreadystatechange = function()
{    
    if(document.readyState=="complete")
    {
        System.Gadget.settingsUI = "Settings.html";
        jQuery.support.cors = true;
        var update = $("#update");
        update.hover(
            function () {$(this).addClass("hover");},
            function () {$(this).removeClass("hover");}
        );
        
        Lojong.updateSloganFromLast();
        Lojong.updateSlogan();
    }        
}
