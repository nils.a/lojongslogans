var updateSlogan,
  componentCompleted,
  configChanged;
  
updateSlogan = function() {
  "use strict";
  var xhr = new XMLHttpRequest();
  
  xhr.open("GET","http://lojong.nils-andresen.de/Slogan", true);
  xhr.onreadystatechange = function() {
    var jsonObject;
    if (xhr.readyState === xhr.DONE) {
      if (xhr.status === 200) {
        json = eval('(' + xhr.responseText + ')');
	console.log("Got json: " + xhr.responseText );
        slogan.text = json[0].slogan;
	status.text = "Updated at: " + new Date();
      } else {
	console.dir(xhr);
	status.text = "Error: " + xhr.status + "(" + xhr.statusText + ")";
      }
    }
  };
  
  xhr.send();  
};

componentCompleted = function() {
  var updateInterval;
  console.log("component Loaded...");
  updateInterval = plasmoid.readConfig("updateInterval");
  console.log("timeout: " + updateInterval + " hours.");
  updateTimer.interval = updateInterval * 360000;
  plasmoid.configChanged = configChanged;  
  updateSlogan();  
};

configChanged = function() {
  var updateInterval;
  updateInterval = plasmoid.readConfig("updateInterval");
  console.log("updating timeout to: " + updateInterval + " hours.");
  updateTimer.interval = updateInterval * 360000;
};

