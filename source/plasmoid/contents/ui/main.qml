import QtQuick 1.0
import org.kde.plasma.core 0.1 as PlasmaCore
import "plasmapackage:/code/script.js" as Script

Item {
    width: 170
    height: 50
     
    Component.onCompleted: {
        Script.componentCompleted();
    }
    
    Text {      
        id: first
        font.pointSize: 12
        text: i18n("Lojong-Slogan of the day:")
        anchors { 
            top: parent.top;
            left: parent.left;
            right: parent.right;
        }
    }
    Text {
        id: slogan
        Timer {
	    id: updateTimer
            interval: 360000; running: true; repeat: true
            onTriggered: Script.updateSlogan()
        }
        text: i18n("missing...")
	wrapMode: TextEdit.WordWrap
        anchors {
	    margins: 10
            top: first.bottom;
            left: parent.left;
            right: parent.right;
            bottom: parent.bottom;
        }
    }
    Text {
        id: status
        text: i18n("Status")
        anchors {
            left: parent.left;
            right: parent.right;
            bottom: parent.bottom;
        }
    }
}
