$(function () {
    "use strict";
    var startUpdate, doUpdate,
    timeout = 3600000;

    doUpdate = function () {
        $.getJSON("http://lojong.nils-andresen.de/Slogan")
            .done(function(json) {
                console.log(json);
                $('#slogan').text(json[0].slogan);
                $('#footer').text("Updated at: " + $.format.date(new Date()));
            })
            .fail(function(jqxhr, textStatus, error) {
                console.log("Error ["+ error +"] updating: "+ textStatus);
                $('#footer').text("with Error at: " + $.format.date(new Date()));
            });
    };

    startUpdate = function () {
        doUpdate();
        setTimeout(startUpdate, timeout);
    }

    startUpdate();
});


